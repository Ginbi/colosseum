public class Rogue extends Character{

    public Rogue(String name) {
        super(name);
        this.className = "Rogue";
        this.healthPoints = 20;
    }

    @Override
    public void attack(Character target){

        int damage = 0;

        switch(target.getClassName()){
            case "Rogue":
            case "Fighter":
                damage = 4;
                break;

            case "Wizard":
                damage = 8;
                break;

            default:
                System.out.println("The attack missed !");
                break;
        }
        System.out.println(this.getName() + " throws a dagger at " + target.getName());
        target.takeDamage(damage);
    }

    @Override
    public void takeDamage(int damage){
        if(hasGuardOn){
            damage /= 2;
        }

        this.setHealthPoints(damage);
        this.hasGuardOn = false;
        System.out.println(this.getName() + " took " + damage + " points of damage !");
    }

    @Override
    public void guard(){
        this.hasGuardOn = true;
        System.out.println(this.getName() + " is hiding in the shadows");
    }

    @Override
    public void special(Character target){
        if(!isCharging){
            this.isCharging = true;
            System.out.println(this.getName() + " seems to flee...");

        } else {
            int damage = 0;

            switch(target.getClassName()){
                case "Rogue":
                case "Fighter":
                    damage = 8;
                    break;

                case "Wizard":
                    damage = 10;
                    break;

                default:
                    System.out.println("The attack missed !");
                    break;
            }
            System.out.println(this.getName() + " hits " + target.getName() + " from behind !");
            this.isCharging = false;
            target.takeDamage(damage);
        }
        
    }

    
}
