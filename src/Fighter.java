public class Fighter extends Character{

    public Fighter(String name) {
        super(name);
        this.className = "Fighter";
        this.healthPoints = 30;
    }

    @Override
    public void attack(Character target){

        int damage = 0;

        switch(target.getClassName()){
            case "Fighter":
            case "Wizard":
                damage = 4;
                break;

            case "Rogue":
                damage = 8;
                break;

            default:
                System.out.println("The attack missed !");
                break;
        }
        System.out.println(this.getName() + " punches " + target.getName());
        target.takeDamage(damage);
    }

    @Override
    public void takeDamage(int damage){
        if(hasGuardOn){
            damage /= 2;
        }

        this.setHealthPoints(damage);
        this.hasGuardOn = false;
        System.out.println(this.getName() + " took " + damage + " points of damage !");
    }

    @Override
    public void guard(){
        this.hasGuardOn = true;
        System.out.println(this.getName() + " puts his guard on !");
    }

    @Override
    public void special(Character target){
        if(!isCharging){
            this.isCharging = true;
            System.out.println(this.getName() + " starts spinning on himslef...");

        } else {
            int damage = 0;

            switch(target.getClassName()){
                case "Fighter":
                case "Wizard":
                    damage = 8;
                    break;

                case "Rogue":
                    damage = 10;
                    break;

                default:
                    System.out.println("The attack missed !");
                    break;
            }

            System.out.println(this.getName() + " lands a roundhouse kick on " + target.getName());
            this.isCharging = false;
            target.takeDamage(damage);
        }
        
        
    }
}


