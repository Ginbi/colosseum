public class Wizard extends Character{

    public Wizard(String name) {
        super(name);
        this.className = "Wizard";
        this.healthPoints = 10;
    }

    @Override
    public void attack(Character target){

        int damage = 0;

        switch(target.getClassName()){
            case "Wizard":
            case "Rogue":
                damage = 4;
                break;

            case "Fighter":
                damage = 8;
                break;

            default:
                System.out.println("The attack missed !");
                break;
        }
        System.out.println(this.getName() + " casts a spell on " + target.getName());
        target.takeDamage(damage);
        
    }

    @Override
    public void takeDamage(int damage){
        if(hasGuardOn){
            damage /= 2;
        }

        this.setHealthPoints(damage);
        this.hasGuardOn = false;
        System.out.println(this.getName() + " took " + damage + " points of damage !");
    }
    @Override
    public void guard(){
        this.hasGuardOn = true;
        System.out.println(this.getName() + " casts a shielding spell !");
    }

    @Override
    public void special(Character target){
        if(!isCharging){
            System.out.println(this.getClassName() + " is charging up a spell...");
            this.isCharging = true;

        } else {
            int damage = 0;

            switch(target.getClassName()){
                case "Wizard":
                case "Rogue":
                    damage = 8;
                    break;

                case "Fighter":
                    damage = 10;
                    break;

                default:
                    System.out.println("The attack missed !");
                    break;
            }
            System.out.println(this.getName() + " casts a huge fireball on " + target.getName());
            this.isCharging = false;
            target.takeDamage(damage);
        }

    }

    
}
