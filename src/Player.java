import java.util.Scanner;

public class Player {
	
	Scanner userInput = new Scanner(System.in);

	public static Character[] characters = new Character[2];
	
	private int id;
	
	public Player(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}

	
	public Character[] createCharacter() {

		System.out.println("Player " + (this.id + 1)+ ", choose a name for your character:");
		String characterName = userInput.nextLine();
		String characterClass = "default";
		boolean hasValidClass = false;
		
		do {
			System.out.println("Choose a class for " + characterName + " : Wizard, Fighter or Rogue");
			characterClass = userInput.nextLine().toUpperCase();
			
			switch (characterClass) {
			
				case "W":
				case "WIZARD":
					characters[this.id] = new Wizard(characterName);
					hasValidClass = true;
					break;
					
				case "F":
				case "FIGHTER":
					characters[this.id] = new Fighter(characterName);
					hasValidClass = true;
					break;
					
				case "R":
				case "ROGUE":
				characters[this.id] = new Rogue(characterName);
					hasValidClass = true;
					break;
					
				default:
					System.out.println("Invalid class selected");
			}
		} while(!hasValidClass);
		
		return characters;
		
	}

}
