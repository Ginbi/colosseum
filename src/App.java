import java.util.Scanner;

public class App {
	public static void main (String [] args) {
		
		System.out.println("Welcome to the Colosseum !");
 
		Player player1 = new Player(0);
		Player player2 = new Player(1);

		player1.createCharacter();
		
		player2.createCharacter();

		System.out.println(Player.characters[0].getName() + " is a " + Player.characters[0].getClassName());
		System.out.println(Player.characters[1].getName() + " is a " + Player.characters[1].getClassName());

		System.out.println("Your characters are created, let's make them fight !");

		boolean fightIsOver = false;

		Scanner actionInput = new Scanner(System.in);

		while (!fightIsOver){

			int currentIndex = 0;

			for(Character character : Player.characters){
	
				String characterAction;
				boolean characterDoesAction = false;
				Character target = Player.characters[(currentIndex + 1) % 2];

				System.out.println(character.getName() + " has " + character.getHealthPoints() + "HP");

				if(character.getIsCharging()){
					character.special(target);
				} else {

					do {
						System.out.println("Attack, Guard, Special");
						characterAction = actionInput.nextLine().toUpperCase();
						
						switch (characterAction) {
						
							case "A":
							case "ATTACK":
								character.attack(target);
								characterDoesAction = true;
								break;
								
							case "G":
							case "GUARD":
								character.guard();
								characterDoesAction = true;
								break;
								
							case "S":
							case "SPECIAL":
								character.special(target);
								characterDoesAction = true;
								break;
								
							default:
								System.out.println("Invalid action selected");
						}
					} while(!characterDoesAction);
				}

				if (target.getHealthPoints() <= 0){
					fightIsOver = true;
					System.out.println(character.getName() + " won the fight !");
					break;
				}

				currentIndex = (currentIndex + 1) % 2;
			} 
		
		}
		
		actionInput.close();

	}
}
