
abstract public class Character{
	
	private String name;
	protected String className;
	protected int healthPoints;
	protected boolean hasGuardOn = false;
	protected boolean isCharging = false;
	

	public Character(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClassName() {
		return this.className;
	}

	public int getHealthPoints() {
		return this.healthPoints;
	}

	public void setHealthPoints(int damage) {
		this.healthPoints -= damage;
	}

	public boolean getGuard() {
		return this.hasGuardOn;
	}

	public boolean getIsCharging(){
		return this.isCharging;
	}

	abstract public void attack(Character target);

	abstract public void takeDamage(int damage);

    abstract public void guard();

    abstract public void special(Character target);
}